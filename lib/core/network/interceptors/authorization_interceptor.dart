import 'package:bee_order_test/core/general_exports.dart';
import 'package:dio/dio.dart';

class AuthorizationInterceptor extends Interceptor {
  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    var token = await getToken();
    options.headers['Authorization'] = 'Bearer $token';

    super.onRequest(options, handler);
  }
}

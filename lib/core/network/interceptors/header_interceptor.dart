import 'package:dio/dio.dart';

class HeaderInterceptor extends Interceptor {
  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {

    options.headers['version'] = '4.0.6';
    options.headers['Connection'] = 'keep-alive';
    options.headers['device_id'] = 'eT8zP0H-RW-X89o7zBLUcZ:APA91bEpc7ghF4EoWNILCHs6r0VtYdWFFX_x61IqH-dNRiMbXcyVoOzwlxlQEUKqenbzNJDYPgJDORqgYmCb-F6LiCXtcjwT8s07Q_E0-f7ufwgeEeKZtDm9dWekMFi1rDgyhcFbyyfl';
    super.onRequest(options, handler);
  }
}

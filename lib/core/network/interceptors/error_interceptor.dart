import 'package:bee_order_test/core/network/common_model/inner_model.dart';
import 'package:dio/dio.dart';

class InnErrorInterceptors extends Interceptor {
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    var model = innerModelFromJson(response.toString());
    if (model.success != true) {
      DioError dioError = DioError(
        requestOptions: response.requestOptions,
        type: DioErrorType.cancel,
        response: response,
      );
      return handler.reject(dioError);
    }

    return super.onResponse(response, handler);
  }
}

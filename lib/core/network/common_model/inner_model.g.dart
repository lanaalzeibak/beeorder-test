// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inner_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_InnerModel _$$_InnerModelFromJson(Map<String, dynamic> json) =>
    _$_InnerModel(
      success: json['success'] as bool,
      code: json['code'] as String?,
      message: json['message'] as String?,
    );

Map<String, dynamic> _$$_InnerModelToJson(_$_InnerModel instance) =>
    <String, dynamic>{
      'success': instance.success,
      'code': instance.code,
      'message': instance.message,
    };

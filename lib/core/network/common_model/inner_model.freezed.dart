// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'inner_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

InnerModel _$InnerModelFromJson(Map<String, dynamic> json) {
  return _InnerModel.fromJson(json);
}

/// @nodoc
mixin _$InnerModel {
  bool get success => throw _privateConstructorUsedError;
  String? get code => throw _privateConstructorUsedError;
  String? get message => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $InnerModelCopyWith<InnerModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InnerModelCopyWith<$Res> {
  factory $InnerModelCopyWith(
          InnerModel value, $Res Function(InnerModel) then) =
      _$InnerModelCopyWithImpl<$Res, InnerModel>;
  @useResult
  $Res call({bool success, String? code, String? message});
}

/// @nodoc
class _$InnerModelCopyWithImpl<$Res, $Val extends InnerModel>
    implements $InnerModelCopyWith<$Res> {
  _$InnerModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? success = null,
    Object? code = freezed,
    Object? message = freezed,
  }) {
    return _then(_value.copyWith(
      success: null == success
          ? _value.success
          : success // ignore: cast_nullable_to_non_nullable
              as bool,
      code: freezed == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      message: freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_InnerModelCopyWith<$Res>
    implements $InnerModelCopyWith<$Res> {
  factory _$$_InnerModelCopyWith(
          _$_InnerModel value, $Res Function(_$_InnerModel) then) =
      __$$_InnerModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool success, String? code, String? message});
}

/// @nodoc
class __$$_InnerModelCopyWithImpl<$Res>
    extends _$InnerModelCopyWithImpl<$Res, _$_InnerModel>
    implements _$$_InnerModelCopyWith<$Res> {
  __$$_InnerModelCopyWithImpl(
      _$_InnerModel _value, $Res Function(_$_InnerModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? success = null,
    Object? code = freezed,
    Object? message = freezed,
  }) {
    return _then(_$_InnerModel(
      success: null == success
          ? _value.success
          : success // ignore: cast_nullable_to_non_nullable
              as bool,
      code: freezed == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      message: freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_InnerModel implements _InnerModel {
  const _$_InnerModel({required this.success, this.code, this.message});

  factory _$_InnerModel.fromJson(Map<String, dynamic> json) =>
      _$$_InnerModelFromJson(json);

  @override
  final bool success;
  @override
  final String? code;
  @override
  final String? message;

  @override
  String toString() {
    return 'InnerModel(success: $success, code: $code, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InnerModel &&
            (identical(other.success, success) || other.success == success) &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.message, message) || other.message == message));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, success, code, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InnerModelCopyWith<_$_InnerModel> get copyWith =>
      __$$_InnerModelCopyWithImpl<_$_InnerModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_InnerModelToJson(
      this,
    );
  }
}

abstract class _InnerModel implements InnerModel {
  const factory _InnerModel(
      {required final bool success,
      final String? code,
      final String? message}) = _$_InnerModel;

  factory _InnerModel.fromJson(Map<String, dynamic> json) =
      _$_InnerModel.fromJson;

  @override
  bool get success;
  @override
  String? get code;
  @override
  String? get message;
  @override
  @JsonKey(ignore: true)
  _$$_InnerModelCopyWith<_$_InnerModel> get copyWith =>
      throw _privateConstructorUsedError;
}

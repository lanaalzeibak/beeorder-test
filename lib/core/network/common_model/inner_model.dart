// To parse this JSON data, do
//
//     final innerModel = innerModelFromJson(jsonString);

import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'inner_model.freezed.dart';
part 'inner_model.g.dart';

InnerModel innerModelFromJson(String str) => InnerModel.fromJson(json.decode(str));

String innerModelToJson(InnerModel data) => json.encode(data.toJson());

@freezed
abstract class InnerModel with _$InnerModel {
  const factory InnerModel({
    required bool success,
    String? code,
    String? message,
  }) = _InnerModel;

  factory InnerModel.fromJson(Map<String, dynamic> json) => _$InnerModelFromJson(json);
}

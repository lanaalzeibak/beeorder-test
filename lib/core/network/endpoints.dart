class Endpoints {
  Endpoints._();

  static const String baseURL = 'https://dev-delivery-v4.lemonilab.com/api';
  static const String baseURLMap = 'https://dev-webdash.lemonilab.com/api';

  static const int receiveTimeout = 5000000;
  static const int connectionTimeout = 3000000;


}

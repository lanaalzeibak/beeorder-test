import 'package:bee_order_test/core/general_exports.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_cubit.freezed.dart';
part 'app_state.dart';

class AppCubit extends Cubit<AppState> {
  AppCubit() : super(const AppState.init());

  Future<void> checkAuth() async {
    try {
      emit(const AppState.appLoading());
      final logged = (await getToken()).isNotEmpty;
      if (logged) {
        emit(const AppState.authenticated());
      } else {
        emit(const AppState.unAuthenticated());
      }
    } on Exception catch (e) {
      emit(AppState.failure(message: e.toString().split(':')[1]));
    }
  }
}

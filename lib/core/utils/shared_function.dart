import 'package:bee_order_test/core/general_exports.dart';

logoutConfirmed() async {
  SharedFunction().removeShared(PrefKeys.tokenLoggedIn);
  AutoRouter.of(navigatorKey.currentContext!)
      .replaceAll([const LoginInPageRoute()]);
}

Future<String> getToken() async {
  return await SharedFunction().getShared(PrefKeys.tokenLoggedIn) ?? "";
}

setToken() async {
  return await SharedFunction().setShared(PrefKeys.tokenLoggedIn, "true");
}

class SharedFunction {
  getShared(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key) ?? "";
  }

  getSharedBool(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key) ?? false;
  }

  setShared(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  removeShared(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(key);
  }
}

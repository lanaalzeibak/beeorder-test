
String? validateRequired(String? val) {
  return (val == null || val.trim().isEmpty) ? "required" : null;
}

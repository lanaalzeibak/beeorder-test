import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../general_exports.dart';


showToast({required String msg, required bool isError, Toast? length}) =>
    Fluttertoast.showToast(
      msg: msg,
      toastLength: length ?? Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: isError ? theme.primaryColor : Colors.green[400],
      textColor: theme.cardColor,
      fontSize: 18.0,
    );

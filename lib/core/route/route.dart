import 'package:auto_route/auto_route.dart';
import 'package:bee_order_test/core/common/home/app_view.dart';
import 'package:bee_order_test/features/add_rest/presentation/add_rest_page.dart';
import 'package:bee_order_test/features/home/presentation/home_page.dart';
import 'package:bee_order_test/features/login/presentation/login_page.dart';
import 'package:bee_order_test/features/nearby/presentation/nearby_page.dart';
import 'package:bee_order_test/features/setting/presentation/setting_page.dart';
import 'package:bee_order_test/features/signup/presentation/signup_page.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    CustomRoute(
      path: "/",
      page: AppView,
      initial: true,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      path: "/loginInView",
      name: 'loginInPageRoute',
      page: LogInPage,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      path: "/signUpView",
      name: 'signUpPageRoute',
      page: SignUpPage,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      path: "/settingView",
      name: 'settingPageRoute',
      page: SettingPage,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      path: "/addRestaurantView",
      name: 'addRestaurantPageRoute',
      page: AddResPage,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      path: "/nearbyView",
      name: 'NearbyPageRoute',
      page: NearbyPage,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
  ],
)
class $AppRouter {}

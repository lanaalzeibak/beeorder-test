// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i7;
import 'package:flutter/material.dart' as _i8;

import '../../features/add_rest/presentation/add_rest_page.dart' as _i5;
import '../../features/login/presentation/login_page.dart' as _i2;
import '../../features/nearby/presentation/nearby_page.dart' as _i6;
import '../../features/setting/presentation/setting_page.dart' as _i4;
import '../../features/signup/presentation/signup_page.dart' as _i3;
import '../common/home/app_view.dart' as _i1;

class AppRouter extends _i7.RootStackRouter {
  AppRouter([_i8.GlobalKey<_i8.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i7.PageFactory> pagesMap = {
    AppView.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i1.AppView(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    LoginInPageRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i2.LogInPage(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    SignUpPageRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i3.SignUpPage(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    SettingPageRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i4.SettingPage(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    AddRestaurantPageRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i5.AddResPage(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    NearbyPageRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i6.NearbyPage(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
  };

  @override
  List<_i7.RouteConfig> get routes => [
        _i7.RouteConfig(
          AppView.name,
          path: '/',
        ),
        _i7.RouteConfig(
          LoginInPageRoute.name,
          path: '/loginInView',
        ),
        _i7.RouteConfig(
          SignUpPageRoute.name,
          path: '/signUpView',
        ),
        _i7.RouteConfig(
          SettingPageRoute.name,
          path: '/settingView',
        ),
        _i7.RouteConfig(
          AddRestaurantPageRoute.name,
          path: '/addRestaurantView',
        ),
        _i7.RouteConfig(
          NearbyPageRoute.name,
          path: '/nearbyView',
        ),
      ];
}

/// generated route for
/// [_i1.AppView]
class AppView extends _i7.PageRouteInfo<void> {
  const AppView()
      : super(
          AppView.name,
          path: '/',
        );

  static const String name = 'AppView';
}

/// generated route for
/// [_i2.LogInPage]
class LoginInPageRoute extends _i7.PageRouteInfo<void> {
  const LoginInPageRoute()
      : super(
          LoginInPageRoute.name,
          path: '/loginInView',
        );

  static const String name = 'LoginInPageRoute';
}

/// generated route for
/// [_i3.SignUpPage]
class SignUpPageRoute extends _i7.PageRouteInfo<void> {
  const SignUpPageRoute()
      : super(
          SignUpPageRoute.name,
          path: '/signUpView',
        );

  static const String name = 'SignUpPageRoute';
}

/// generated route for
/// [_i4.SettingPage]
class SettingPageRoute extends _i7.PageRouteInfo<void> {
  const SettingPageRoute()
      : super(
          SettingPageRoute.name,
          path: '/settingView',
        );

  static const String name = 'SettingPageRoute';
}

/// generated route for
/// [_i5.AddResPage]
class AddRestaurantPageRoute extends _i7.PageRouteInfo<void> {
  const AddRestaurantPageRoute()
      : super(
          AddRestaurantPageRoute.name,
          path: '/addRestaurantView',
        );

  static const String name = 'AddRestaurantPageRoute';
}

/// generated route for
/// [_i6.NearbyPage]
class NearbyPageRoute extends _i7.PageRouteInfo<void> {
  const NearbyPageRoute()
      : super(
          NearbyPageRoute.name,
          path: '/nearbyView',
        );

  static const String name = 'NearbyPageRoute';
}

part of 'login_bloc.dart';

@immutable
abstract class LogInEvent {}

class LogIn extends LogInEvent {
  final String phoneNumber;

  LogIn({required this.phoneNumber});
}

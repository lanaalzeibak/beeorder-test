import 'package:bee_order_test/features/login/data/login_repo.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:bee_order_test/core/general_exports.dart';

part 'login_event.dart';

part 'login_state.dart';

part 'login_bloc.freezed.dart';

class LogInBloc extends Bloc<LogIn, LoginState> {
  LogInRepository repo;

  LogInBloc(this.repo) : super(const Initial()) {
    on<LogIn>(_checkLogin);
  }

  void _checkLogin(LogIn event, Emitter<LoginState> emit) async {
    try {
      emit(const LoginState.loading());
      var res = await repo.checkLogIn(event.phoneNumber);
      if (res.success) {
        await setToken();
      }
      emit(const LoginState.loginInLoaded());
    } catch (e) {
      emit(LoginState.failure(message: e.toString()));
    }
  }
}

import 'package:bee_order_test/core/general_exports.dart';
import 'package:bee_order_test/core/utils/toast.dart';
import 'package:bee_order_test/features/login/bloc/login_bloc/login_bloc.dart';
import 'package:bee_order_test/features/login/data/login_repo.dart';
import 'package:bee_order_test/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LogInPage extends StatelessWidget {
  const LogInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LogInBloc(LogInRepository()),
      child: const LogInView(),
    );
  }
}

class LogInView extends StatefulWidget {
  const LogInView({Key? key}) : super(key: key);

  @override
  State<LogInView> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInView> {
  TextEditingController phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<LogInBloc, LoginState>(
        listener: (context, state) async {
          if (state is loginInLoaded) {
            AutoRouter.of(context).replaceAll([const AddRestaurantPageRoute()]);
          }
        },
        builder: (context, state) {
          if (state is Loading) {
            return const LoadingWidget();
          }
          return buildWidget();
        },
      ),
    );
  }

  Widget buildWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(height: 50.sp),
        Image.asset(
          Assets.logo.path,
          height: 45.sp,
        ),
        SizedBox(height: 40.sp),
        Container(
          margin: EdgeInsets.all(25.sp),
          child: TextFormField(
            controller: phoneController,
            keyboardType: TextInputType.number,
            decoration: const InputDecoration()
                .applyDefaults(theme.inputDecorationTheme)
                .copyWith(labelText: 'Phone number', hintText: "09"),
            onFieldSubmitted: (val) {
              callLogIn();
            },
          ),
        ),
        SizedBox(
          height: 25.sp,
          width: 50.sp,
          child: ElevatedButton(
            onPressed: () {
              callLogIn();
            },
            child: const Text('login'),
          ),
        ),
        SizedBox(height: 15.sp),
        InkWell(
          onTap: () {
            AutoRouter.of(context).push(const SignUpPageRoute());
          },
          child: const Text(
            "Create account",
            style: TextStyle(
              decoration: TextDecoration.underline,
            ),
          ),
        ),
      ],
    );
  }

  callLogIn() {
    if (phoneController.text.trim().isEmpty) {
      showToast(msg: "Phone number is required", isError: true);
    } else if (phoneController.text.trim().length != 10 &&
        !phoneController.text.trim().startsWith('09')) {
      showToast(msg: "Please enter valid phone number", isError: true);
    } else {
      final bloc = BlocProvider.of<LogInBloc>(context);
      String phoneNumber = phoneController.text.trim().substring(1);
      bloc.add(LogIn(phoneNumber: phoneNumber));
    }
  }

  @override
  void dispose() {
    phoneController.dispose();

    super.dispose();
  }
}

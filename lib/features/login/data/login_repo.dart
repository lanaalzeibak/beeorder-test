import 'package:bee_order_test/features/login/data/login_api.dart';
import 'package:bee_order_test/features/login/data/login_model/login_model.dart';

class LogInRepository {
  DioLogIn dio = DioLogIn();

  Future<LoginModel> checkLogIn(String phoneNumber) async {
    try {
      var res = await dio.login(phoneNumber);
      return res;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}

import 'package:bee_order_test/core/network/dio_exception.dart';
import 'package:bee_order_test/core/network/endpoints.dart';
import 'package:bee_order_test/features/login/data/login_model/login_model.dart';
import 'package:dio/dio.dart';

import '../../../core/network/interceptors/interceptors.dart';

class DioLogIn {
  DioLogIn()
      : _dio = Dio(
          BaseOptions(
            baseUrl: Endpoints.baseURL,
            connectTimeout: Endpoints.connectionTimeout,
            receiveTimeout: Endpoints.receiveTimeout,
            responseType: ResponseType.json,
          ),
        )..interceptors.addAll([
            LoggerInterceptor(),
            LanguageInterceptor(),
            InnErrorInterceptors(),
            HeaderInterceptor(),
          ]);

  late final Dio _dio;

  Future<LoginModel> login(String phoneNumber) async {
    try {
      final response = await _dio.post('/login', data: '{"mobile": $phoneNumber}');
      return loginModelFromJson(response.toString());
    } on DioError catch (err) {
      String errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    }
  }
}

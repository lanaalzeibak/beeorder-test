import 'dart:ffi';

import 'package:bee_order_test/core/utils/location.dart';
import 'package:bee_order_test/core/utils/toast.dart';
import 'package:bee_order_test/features/setting/data/setting_repo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';

import '../../../core/general_exports.dart';

part 'setting_event.dart';

part 'setting_state.dart';

part 'setting_bloc.freezed.dart';

class SettingBloc extends Bloc<SendNotification, SettingState> {
  SettingRepository repo;

  SettingBloc(this.repo) : super(const Initial()) {
    on<SendNotification>(_sendNotification);
  }

  void _sendNotification(
      SendNotification event, Emitter<SettingState> emit) async {
    try {
      emit(const SettingState.loading());
      var res = await repo.getSetting(event.message);
      showToast(msg: res.result, isError: false);
      emit(const SettingState.settingInLoaded());
    } catch (e) {
      emit(SettingState.failure(message: e.toString()));
    }
  }
}

part of 'setting_bloc.dart';

@immutable
abstract class SettingEvent {}

class SendNotification extends SettingEvent {
  final String message;
  SendNotification(this.message);
}

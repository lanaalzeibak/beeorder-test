import 'package:bee_order_test/core/network/dio_exception.dart';
import 'package:bee_order_test/core/network/endpoints.dart';
import 'package:bee_order_test/features/setting/data/notification_model/notification_model.dart';
import 'package:dio/dio.dart';

import '../../../core/network/interceptors/interceptors.dart';

class DioSetting {
  DioSetting()
      : _dio = Dio(
          BaseOptions(
            baseUrl: Endpoints.baseURLMap,
            connectTimeout: Endpoints.connectionTimeout,
            receiveTimeout: Endpoints.receiveTimeout,
            responseType: ResponseType.json,
          ),
        )..interceptors.addAll([
            LoggerInterceptor(),
            HeaderInterceptor(),
          ]);

  late final Dio _dio;

  Future<NotificationModel> sendNotification(String message) async {
    try {
      final response = await _dio.get(
          '/coreservices/index.php/home/send_driver_notification?format=json&driver_id=1&message=$message');
      return notificationModelFromJson(response.toString());
    } on DioError catch (err) {
      String errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    }
  }
}

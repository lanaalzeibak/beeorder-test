import 'package:bee_order_test/features/setting/data/notification_model/notification_model.dart';
import 'package:bee_order_test/features/setting/data/setting_api.dart';

class SettingRepository {
  DioSetting dio = DioSetting();

  Future<NotificationModel> getSetting(String message) async {
    try {
      var res = await dio.sendNotification(message);
      return res;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}

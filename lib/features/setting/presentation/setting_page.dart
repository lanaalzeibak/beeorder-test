import 'package:bee_order_test/core/general_exports.dart';
import 'package:bee_order_test/features/home/presentation/home_page.dart';
import 'package:bee_order_test/features/setting/bloc/setting_bloc.dart';
import 'package:bee_order_test/features/setting/data/setting_repo.dart';
import 'package:flutter/material.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SettingBloc(SettingRepository()),
      child: const SettingView(),
    );
  }
}

class SettingView extends StatefulWidget {
  const SettingView({Key? key}) : super(key: key);

  @override
  State<SettingView> createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {
  final TextEditingController controller = TextEditingController();

  callBloc() {
    final bloc = BlocProvider.of<SettingBloc>(context);
    bloc.add(SendNotification(controller.text));
  }

  @override
  Widget build(BuildContext context) {
    return HomePage(
      selectedPos: BottomNav.setting.index,
      titleAppBar: "Setting",
      body: BlocConsumer<SettingBloc, SettingState>(
        listener: (context, state) async {
          if (state is settingInLoaded) {
            setState(() {
              controller.text = "";
            });
          }
        },
        builder: (context, state) {
          return columnWidget();
        },
      ),
    );
  }

  Widget columnWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: SizedBox(
            height: 25.sp,
            width: 60.sp,
            child: ElevatedButton(
              onPressed: () {
                logoutConfirmed();
              },
              child: const Text('Log out'),
            ),
          ),
        ),
        SizedBox(height: 20.sp),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20.sp, vertical: 10.sp),
          child: TextFormField(
            controller: controller,
            decoration: const InputDecoration()
                .applyDefaults(theme.inputDecorationTheme)
                .copyWith(labelText: 'Notification message'),
          ),
        ),
        SizedBox(height: 10.sp),
        Center(
          child: SizedBox(
            height: 25.sp,
            width: 60.sp,
            child: ElevatedButton(
              onPressed: () {
                callBloc();
              },
              child: const Text('send notification'),
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

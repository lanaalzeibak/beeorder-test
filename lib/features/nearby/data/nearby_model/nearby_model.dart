// To parse this JSON data, do
//
//     final nearbyModel = nearbyModelFromJson(jsonString);

import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'nearby_model.freezed.dart';

part 'nearby_model.g.dart';

NearbyModel nearbyModelFromJson(String str) =>
    NearbyModel.fromJson(json.decode(str));

String nearbyModelToJson(NearbyModel data) => json.encode(data.toJson());

@freezed
abstract class NearbyModel with _$NearbyModel {
  const factory NearbyModel({
    List<Result>? result,
  }) = _NearbyModel;

  factory NearbyModel.fromJson(Map<String, dynamic> json) =>
      _$NearbyModelFromJson(json);
}

@freezed
abstract class Result with _$Result {
  const factory Result({
    required String name,
    required String latitude,
    required String longitude,
  }) = _Result;

  factory Result.fromJson(Map<String, dynamic> json) => _$ResultFromJson(json);
}

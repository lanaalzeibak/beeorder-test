import 'package:bee_order_test/core/network/dio_exception.dart';
import 'package:bee_order_test/core/network/endpoints.dart';
import 'package:bee_order_test/features/nearby/data/nearby_model/nearby_model.dart';
import 'package:dio/dio.dart';

import '../../../core/network/interceptors/interceptors.dart';

class DioNearby {
  DioNearby()
      : _dio = Dio(
          BaseOptions(
            baseUrl: Endpoints.baseURLMap,
            connectTimeout: Endpoints.connectionTimeout,
            receiveTimeout: Endpoints.receiveTimeout,
            responseType: ResponseType.json,
          ),
        )..interceptors.addAll([
            LoggerInterceptor(),
            HeaderInterceptor(),
          ]);

  late final Dio _dio;

  Future<NearbyModel> getNearby() async {
    try {
      final response = await _dio
          .get('/coreservices/index.php/home/get_restaurant?format=json');
      return nearbyModelFromJson(response.toString());
    } on DioError catch (err) {
      String errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    }
  }
}

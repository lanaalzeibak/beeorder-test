
import 'package:bee_order_test/features/nearby/data/nearby_api.dart';
import 'package:bee_order_test/features/nearby/data/nearby_model/nearby_model.dart';

class NearbyRepository {
  DioNearby dio = DioNearby();

  Future<NearbyModel> getNearby() async {
    try {
      var res = await dio.getNearby();
      return res;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}

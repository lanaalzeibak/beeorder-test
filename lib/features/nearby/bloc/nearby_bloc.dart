import 'dart:ffi';

import 'package:bee_order_test/core/utils/location.dart';
import 'package:bee_order_test/features/nearby/data/nearby_model/nearby_model.dart';
import 'package:bee_order_test/features/nearby/data/nearby_repo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';

import '../../../core/general_exports.dart';

part 'nearby_event.dart';

part 'nearby_state.dart';

part 'nearby_bloc.freezed.dart';

class NearbyBloc extends Bloc<Nearby, NearbyState> {
  NearbyRepository repo;

  NearbyBloc(this.repo) : super(const Initial()) {
    on<Nearby>(_checkNearby);
  }

  void _checkNearby(Nearby event, Emitter<NearbyState> emit) async {
    try {
      emit(const NearbyState.loading());
      NearbyModel res = await repo.getNearby();
      List<Marker> markers = [];

      for (int i = 0; i < (res.result?.length ?? 0); i++) {
        if (res.result != null) {
          var rest = res.result![i];
          markers.add(
            Marker(
              width: 40.sp,
              point: LatLng(
                  double.parse(rest.latitude), double.parse(rest.longitude)),
              builder: (ctx) {
                return Row(
                  children: [
                    Text(
                      rest.name,
                      style: TextStyle(color: Colors.green, fontSize: 15.sp),
                    ),
                    SizedBox(width: 5.sp),
                    Icon(
                      Icons.location_on_rounded,
                      size: 25.sp,
                      color: Colors.green,
                    ),
                  ],
                );
              },
            ),
          );
        }
      }
      emit(NearbyState.nearbyInLoaded(marker: markers));
    } catch (e) {
      emit(NearbyState.failure(message: e.toString()));
    }
  }
}

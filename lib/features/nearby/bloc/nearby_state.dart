part of 'nearby_bloc.dart';

@freezed
class NearbyState with _$NearbyState {
  const factory NearbyState.nearbyInLoaded({
    required  List<Marker> marker,
  }) = nearbyInLoaded;

  const factory NearbyState.loading() = Loading;

  const factory NearbyState.failure({
    required String message,
  }) = Failure;

  const factory NearbyState.init() = Initial;
}

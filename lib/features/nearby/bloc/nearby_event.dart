part of 'nearby_bloc.dart';

@immutable
abstract class NearbyEvent {}

class Nearby extends NearbyEvent {
  Nearby();
}

import 'package:bee_order_test/core/general_exports.dart';
import 'package:bee_order_test/core/utils/location.dart';
import 'package:bee_order_test/core/utils/toast.dart';
import 'package:bee_order_test/features/home/presentation/home_page.dart';
import 'package:bee_order_test/features/nearby/bloc/nearby_bloc.dart';
import 'package:bee_order_test/features/nearby/data/nearby_repo.dart';
import 'package:cron/cron.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';

class NearbyPage extends StatelessWidget {
  const NearbyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => NearbyBloc(NearbyRepository()),
      child: const NearbyView(),
    );
  }
}

class NearbyView extends StatefulWidget {
  const NearbyView({Key? key}) : super(key: key);

  @override
  State<NearbyView> createState() => _NearbyViewState();
}

class _NearbyViewState extends State<NearbyView> {
  List<Marker> marker = [];
  final cron = Cron();

  @override
  void initState() {
    callBloc();
    refreshLocation();
    super.initState();
  }

  refreshLocation() async {
    try {
      cron.schedule(Schedule.parse(' */3 * * * * * '), () async {
        Position res = await determinePosition();
        showToast(
            msg:
                "My location: ${res.latitude.toStringAsFixed(2)}, ${res.longitude.toStringAsFixed(2)} ",
            isError: false);
      });
    } catch (ex) {}
  }

  callBloc() {
    final bloc = BlocProvider.of<NearbyBloc>(context);
    bloc.add(Nearby());
  }

  @override
  Widget build(BuildContext context) {
    return HomePage(
      titleAppBar: "Nearby",
      selectedPos: BottomNav.nearby.index,
      body: BlocConsumer<NearbyBloc, NearbyState>(
        listener: (context, state) async {
          if (state is nearbyInLoaded) {
            setState(() {
              marker = state.marker;
            });
          }
        },
        builder: (context, state) {
          return map();
        },
      ),
    );
  }

  Widget map() {
    return FlutterMap(
      options: MapOptions(
        center: LatLng(33.5074755, 36.2828954),
        zoom: 10,
      ),
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          userAgentPackageName: 'dev.fleaflet.flutter_map.example',
        ),
        MarkerLayer(markers: marker),
      ],
    );
  }

  @override
  void dispose() {
    cron.close();
    super.dispose();
  }
}

import 'package:bee_order_test/core/general_exports.dart';
import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final int selectedPos;
  final String titleAppBar;
  final Widget body;

  const HomePage(
      {required this.selectedPos,
      required this.titleAppBar,
      required this.body,
      super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late CircularBottomNavigationController _navigationController;

  @override
  void initState() {
    super.initState();
    _navigationController =
        CircularBottomNavigationController(widget.selectedPos);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(centerTitle: true, title: Text(widget.titleAppBar)),
      body: widget.body,
      bottomNavigationBar: CircularBottomNavigation(
        getLabelItem(),
        iconsSize: 20,
        barHeight: 50,
        controller: _navigationController,
        selectedPos: widget.selectedPos,
        selectedCallback: (int? selectedPos) {
          changeSelectedNavigation(selectedPos);
        },
      ),
    );
  }

  changeSelectedNavigation(int? selected) {
    if (selected == BottomNav.addRestaurant.index) {
      AutoRouter.of(context).replaceAll([const AddRestaurantPageRoute()]);
    } else if (selected == BottomNav.nearby.index) {
      AutoRouter.of(context).replaceAll([const NearbyPageRoute()]);
    } else if (selected == BottomNav.setting.index) {
      AutoRouter.of(context).replaceAll([const SettingPageRoute()]);
    }
  }
}

import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/material.dart';

import '../../core/general_exports.dart';

getLabelItem() {
  return List.of([
    TabItem(
      Icons.restaurant,
      'add restaurant',
      theme.primaryColor,
    ),
    TabItem(
      Icons.near_me,
      'nearby',
      theme.primaryColor,
    ),
    TabItem(
      Icons.settings,
      'setting',
      theme.primaryColor,
    ),
  ]);
}

enum BottomNav { addRestaurant, nearby, setting }

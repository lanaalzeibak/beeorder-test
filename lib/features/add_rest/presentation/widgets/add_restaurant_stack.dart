import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../core/general_exports.dart';
import '../add_rest_page.dart';

class AddRestaurantStack extends StatefulWidget {
  final AddResViewState state;

  const AddRestaurantStack({Key? key, required this.state}) : super(key: key);

  @override
  State<AddRestaurantStack> createState() => _AddRestaurantStackState();
}

class _AddRestaurantStackState extends State<AddRestaurantStack> {
  final ImagePicker _picker = ImagePicker();
  XFile? images;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: Colors.black.withOpacity(0.3),
      child: Center(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 25.sp),
          padding: EdgeInsets.symmetric(horizontal: 25.sp),
          height: 70.sp,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(radius),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                spreadRadius: 1,
                blurRadius: 5,
                offset: const Offset(1, 3), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            children: [
              SizedBox(height: 20.sp),
              Container(
                margin: EdgeInsets.all(10.sp),
                child: TextFormField(
                  controller: widget.state.nameController,
                  decoration: const InputDecoration()
                      .applyDefaults(theme.inputDecorationTheme)
                      .copyWith(labelText: 'Restaurant name'),
                ),
              ),
              SizedBox(height: 20.sp),
              if (images == null) columnPicker(),
              if (images != null) fileImage(),
              SizedBox(height: 20.sp),
              SizedBox(
                height: 25.sp,
                width: 50.sp,
                child: ElevatedButton(
                  onPressed: () {
                    widget.state.callAddRestBloc();
                  },
                  child: const Text('Save'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget fileImage() {
    return Image.file(
      File(images!.path),
      height: 40.sp,
    );
  }

  Widget columnPicker() {
    return Column(
      children: [
        OutlinedButton(
            onPressed: () async {
              var image = await _picker.pickImage(source: ImageSource.camera);
              if (image != null) {
                setState(() {
                  images = image;
                });
              }
            },
            child: const Text("Upload from camera")),
        SizedBox(height: 20.sp),
        OutlinedButton(
            onPressed: () async {
              var image = await _picker.pickImage(source: ImageSource.gallery);
              if (image != null) {
                setState(() {
                  images = image;
                });
              }
            },
            child: const Text("Upload from Gallery")),
      ],
    );
  }
}

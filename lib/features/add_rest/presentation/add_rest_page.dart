import 'package:bee_order_test/core/general_exports.dart';
import 'package:bee_order_test/core/utils/toast.dart';
import 'package:bee_order_test/features/add_rest/bloc/add_rest_bloc.dart';
import 'package:bee_order_test/features/add_rest/data/add_rest_repo.dart';
import 'package:bee_order_test/features/add_rest/data/model/add_rest_body_model/add_rest_body_model.dart';
import 'package:bee_order_test/features/add_rest/presentation/widgets/add_restaurant_stack.dart';
import 'package:bee_order_test/features/home/presentation/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

class AddResPage extends StatelessWidget {
  const AddResPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AddRestBloc(AddRestRepository()),
      child: const AddResView(),
    );
  }
}

class AddResView extends StatefulWidget {
  const AddResView({Key? key}) : super(key: key);

  @override
  State<AddResView> createState() => AddResViewState();
}

class AddResViewState extends State<AddResView> {
  TextEditingController nameController = TextEditingController();
  List<Marker> marker = [];
  bool _addDialog = false;

  callAddRestBloc() {
    if (nameController.text.trim().isEmpty) {
      showToast(msg: "Restaurant name required", isError: true);
      return;
    }
    AddRestBodyModel body = AddRestBodyModel(
      name: nameController.text,
      latitude: marker[0].point.latitude,
      longitude: marker[0].point.longitude,
      details: "details",
    );
    final bloc = BlocProvider.of<AddRestBloc>(context);
    bloc.add(AddRest(body));
  }

  @override
  Widget build(BuildContext context) {
    return HomePage(
      selectedPos: BottomNav.addRestaurant.index,
      titleAppBar: 'Add restaurant',
      body: BlocConsumer<AddRestBloc, AddRestState>(
        listener: (context, state) async {
          if (state is addRestInLoaded) {
            showToast(msg: state.msg, isError: false);
            setState(() {
              marker = [];
              nameController.text = "";
              _addDialog = false;
            });
          }
        },
        builder: (context, state) {
          if (state is Loading) {
            return const LoadingWidget();
          }
          return mapWidget();
        },
      ),
    );
  }

  Widget mapWidget() {
    return FlutterMap(
      options: MapOptions(
        onTap: (_, lat) {
          debugPrint(lat.toString());
          setState(() {
            marker = [];
            marker.add(
              Marker(
                point: lat,
                builder: (_) {
                  return Icon(
                    Icons.location_on,
                    color: Colors.green,
                    size: 30.sp,
                  );
                },
              ),
            );
          });
        },
        center: LatLng(33.511, 36.33),
        zoom: 15,
      ),
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          userAgentPackageName: 'dev.fleaflet.flutter_map.example',
        ),
        MarkerLayer(markers: marker),
        Container(
          alignment: Alignment.bottomCenter,
          margin: EdgeInsets.only(bottom: 10.sp),
          child: ElevatedButton(
            onPressed: _addRestaurant,
            child: const Text('Add restaurant'),
          ),
        ),
        if (_addDialog) AddRestaurantStack(state: this),
      ],
    );
  }

  _addRestaurant() {
    if (marker.isEmpty) {
      showToast(msg: "Add location first", isError: true);
    } else {
      setState(() {
        _addDialog = true;
      });
    }
  }

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }
}

import 'package:bee_order_test/features/add_rest/data/add_rest_api.dart';
import 'package:bee_order_test/features/add_rest/data/model/add_rest_body_model/add_rest_body_model.dart';
import 'package:bee_order_test/features/add_rest/data/model/add_rest_model/add_rest_model.dart';
import 'package:bee_order_test/features/nearby/data/nearby_api.dart';
import 'package:bee_order_test/features/nearby/data/nearby_model/nearby_model.dart';

class AddRestRepository {
  DioAddRest dio = DioAddRest();

  Future<AddRestModel> addRest(AddRestBodyModel body) async {
    try {
      var res = await dio.addRest(body);
      return res;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}

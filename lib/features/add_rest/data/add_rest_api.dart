import 'package:bee_order_test/core/network/dio_exception.dart';
import 'package:bee_order_test/core/network/endpoints.dart';
import 'package:bee_order_test/features/add_rest/data/model/add_rest_body_model/add_rest_body_model.dart';
import 'package:bee_order_test/features/add_rest/data/model/add_rest_model/add_rest_model.dart';
import 'package:dio/dio.dart';

import '../../../core/network/interceptors/interceptors.dart';

class DioAddRest {
  DioAddRest()
      : _dio = Dio(
          BaseOptions(
            baseUrl: Endpoints.baseURLMap,
            connectTimeout: Endpoints.connectionTimeout,
            receiveTimeout: Endpoints.receiveTimeout,
            responseType: ResponseType.json,
          ),
        )..interceptors.addAll([
            LoggerInterceptor(),
            HeaderInterceptor(),
          ]);

  late final Dio _dio;

  Future<AddRestModel> addRest(AddRestBodyModel body) async {
    try {
      final response = await _dio.post(
        '/coreservices/index.php/home/add_new_restaurant/format/json',
        data: body.toJson(),
      );
      return addRestModelFromJson(response.toString());
    } on DioError catch (err) {
      String errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    }
  }
}

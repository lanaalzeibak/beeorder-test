// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'add_rest_body_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AddRestBodyModel _$AddRestBodyModelFromJson(Map<String, dynamic> json) {
  return _AddRestBodyModel.fromJson(json);
}

/// @nodoc
mixin _$AddRestBodyModel {
  String get name => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  String get details => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AddRestBodyModelCopyWith<AddRestBodyModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddRestBodyModelCopyWith<$Res> {
  factory $AddRestBodyModelCopyWith(
          AddRestBodyModel value, $Res Function(AddRestBodyModel) then) =
      _$AddRestBodyModelCopyWithImpl<$Res, AddRestBodyModel>;
  @useResult
  $Res call({String name, double latitude, double longitude, String details});
}

/// @nodoc
class _$AddRestBodyModelCopyWithImpl<$Res, $Val extends AddRestBodyModel>
    implements $AddRestBodyModelCopyWith<$Res> {
  _$AddRestBodyModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? latitude = null,
    Object? longitude = null,
    Object? details = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: null == latitude
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: null == longitude
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      details: null == details
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AddRestBodyModelCopyWith<$Res>
    implements $AddRestBodyModelCopyWith<$Res> {
  factory _$$_AddRestBodyModelCopyWith(
          _$_AddRestBodyModel value, $Res Function(_$_AddRestBodyModel) then) =
      __$$_AddRestBodyModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String name, double latitude, double longitude, String details});
}

/// @nodoc
class __$$_AddRestBodyModelCopyWithImpl<$Res>
    extends _$AddRestBodyModelCopyWithImpl<$Res, _$_AddRestBodyModel>
    implements _$$_AddRestBodyModelCopyWith<$Res> {
  __$$_AddRestBodyModelCopyWithImpl(
      _$_AddRestBodyModel _value, $Res Function(_$_AddRestBodyModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? latitude = null,
    Object? longitude = null,
    Object? details = null,
  }) {
    return _then(_$_AddRestBodyModel(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: null == latitude
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: null == longitude
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      details: null == details
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AddRestBodyModel implements _AddRestBodyModel {
  const _$_AddRestBodyModel(
      {required this.name,
      required this.latitude,
      required this.longitude,
      required this.details});

  factory _$_AddRestBodyModel.fromJson(Map<String, dynamic> json) =>
      _$$_AddRestBodyModelFromJson(json);

  @override
  final String name;
  @override
  final double latitude;
  @override
  final double longitude;
  @override
  final String details;

  @override
  String toString() {
    return 'AddRestBodyModel(name: $name, latitude: $latitude, longitude: $longitude, details: $details)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AddRestBodyModel &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.latitude, latitude) ||
                other.latitude == latitude) &&
            (identical(other.longitude, longitude) ||
                other.longitude == longitude) &&
            (identical(other.details, details) || other.details == details));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, name, latitude, longitude, details);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AddRestBodyModelCopyWith<_$_AddRestBodyModel> get copyWith =>
      __$$_AddRestBodyModelCopyWithImpl<_$_AddRestBodyModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AddRestBodyModelToJson(
      this,
    );
  }
}

abstract class _AddRestBodyModel implements AddRestBodyModel {
  const factory _AddRestBodyModel(
      {required final String name,
      required final double latitude,
      required final double longitude,
      required final String details}) = _$_AddRestBodyModel;

  factory _AddRestBodyModel.fromJson(Map<String, dynamic> json) =
      _$_AddRestBodyModel.fromJson;

  @override
  String get name;
  @override
  double get latitude;
  @override
  double get longitude;
  @override
  String get details;
  @override
  @JsonKey(ignore: true)
  _$$_AddRestBodyModelCopyWith<_$_AddRestBodyModel> get copyWith =>
      throw _privateConstructorUsedError;
}

// To parse this JSON data, do
//
//     final addRestBodyModel = addRestBodyModelFromJson(jsonString);

import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'add_rest_body_model.freezed.dart';

part 'add_rest_body_model.g.dart';

AddRestBodyModel addRestBodyModelFromJson(String str) =>
    AddRestBodyModel.fromJson(json.decode(str));

String addRestBodyModelToJson(AddRestBodyModel data) =>
    json.encode(data.toJson());

@freezed
abstract class AddRestBodyModel with _$AddRestBodyModel {
  const factory AddRestBodyModel({
    required String name,
    required double latitude,
    required double longitude,
    required String details,
  }) = _AddRestBodyModel;

  factory AddRestBodyModel.fromJson(Map<String, dynamic> json) =>
      _$AddRestBodyModelFromJson(json);
}

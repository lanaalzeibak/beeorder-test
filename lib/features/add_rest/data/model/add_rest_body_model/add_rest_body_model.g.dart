// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_rest_body_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AddRestBodyModel _$$_AddRestBodyModelFromJson(Map<String, dynamic> json) =>
    _$_AddRestBodyModel(
      name: json['name'] as String,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      details: json['details'] as String,
    );

Map<String, dynamic> _$$_AddRestBodyModelToJson(_$_AddRestBodyModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'details': instance.details,
    };

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'add_rest_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AddRestModel _$AddRestModelFromJson(Map<String, dynamic> json) {
  return _AddRestModel.fromJson(json);
}

/// @nodoc
mixin _$AddRestModel {
  int? get code => throw _privateConstructorUsedError;
  String? get msg => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AddRestModelCopyWith<AddRestModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddRestModelCopyWith<$Res> {
  factory $AddRestModelCopyWith(
          AddRestModel value, $Res Function(AddRestModel) then) =
      _$AddRestModelCopyWithImpl<$Res, AddRestModel>;
  @useResult
  $Res call({int? code, String? msg});
}

/// @nodoc
class _$AddRestModelCopyWithImpl<$Res, $Val extends AddRestModel>
    implements $AddRestModelCopyWith<$Res> {
  _$AddRestModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? code = freezed,
    Object? msg = freezed,
  }) {
    return _then(_value.copyWith(
      code: freezed == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as int?,
      msg: freezed == msg
          ? _value.msg
          : msg // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AddRestModelCopyWith<$Res>
    implements $AddRestModelCopyWith<$Res> {
  factory _$$_AddRestModelCopyWith(
          _$_AddRestModel value, $Res Function(_$_AddRestModel) then) =
      __$$_AddRestModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int? code, String? msg});
}

/// @nodoc
class __$$_AddRestModelCopyWithImpl<$Res>
    extends _$AddRestModelCopyWithImpl<$Res, _$_AddRestModel>
    implements _$$_AddRestModelCopyWith<$Res> {
  __$$_AddRestModelCopyWithImpl(
      _$_AddRestModel _value, $Res Function(_$_AddRestModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? code = freezed,
    Object? msg = freezed,
  }) {
    return _then(_$_AddRestModel(
      code: freezed == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as int?,
      msg: freezed == msg
          ? _value.msg
          : msg // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AddRestModel implements _AddRestModel {
  const _$_AddRestModel({this.code, this.msg});

  factory _$_AddRestModel.fromJson(Map<String, dynamic> json) =>
      _$$_AddRestModelFromJson(json);

  @override
  final int? code;
  @override
  final String? msg;

  @override
  String toString() {
    return 'AddRestModel(code: $code, msg: $msg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AddRestModel &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.msg, msg) || other.msg == msg));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, code, msg);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AddRestModelCopyWith<_$_AddRestModel> get copyWith =>
      __$$_AddRestModelCopyWithImpl<_$_AddRestModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AddRestModelToJson(
      this,
    );
  }
}

abstract class _AddRestModel implements AddRestModel {
  const factory _AddRestModel({final int? code, final String? msg}) =
      _$_AddRestModel;

  factory _AddRestModel.fromJson(Map<String, dynamic> json) =
      _$_AddRestModel.fromJson;

  @override
  int? get code;
  @override
  String? get msg;
  @override
  @JsonKey(ignore: true)
  _$$_AddRestModelCopyWith<_$_AddRestModel> get copyWith =>
      throw _privateConstructorUsedError;
}

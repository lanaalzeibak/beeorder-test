// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_rest_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AddRestModel _$$_AddRestModelFromJson(Map<String, dynamic> json) =>
    _$_AddRestModel(
      code: json['code'] as int?,
      msg: json['msg'] as String?,
    );

Map<String, dynamic> _$$_AddRestModelToJson(_$_AddRestModel instance) =>
    <String, dynamic>{
      'code': instance.code,
      'msg': instance.msg,
    };

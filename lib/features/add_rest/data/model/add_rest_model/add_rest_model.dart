// To parse this JSON data, do
//
//     final addRestModel = addRestModelFromJson(jsonString);

import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'add_rest_model.freezed.dart';
part 'add_rest_model.g.dart';

AddRestModel addRestModelFromJson(String str) => AddRestModel.fromJson(json.decode(str));

String addRestModelToJson(AddRestModel data) => json.encode(data.toJson());

@freezed
abstract class AddRestModel with _$AddRestModel {
  const factory AddRestModel({
    int? code,
    String? msg,
  }) = _AddRestModel;

  factory AddRestModel.fromJson(Map<String, dynamic> json) => _$AddRestModelFromJson(json);
}

part of 'add_rest_bloc.dart';

@immutable
abstract class AddRestEvent {}

class AddRest extends AddRestEvent {
  final AddRestBodyModel body;
  AddRest(this.body);
}

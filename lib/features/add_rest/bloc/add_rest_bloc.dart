import 'package:bee_order_test/features/add_rest/data/add_rest_repo.dart';
import 'package:bee_order_test/features/add_rest/data/model/add_rest_body_model/add_rest_body_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../core/general_exports.dart';

part 'add_rest_event.dart';

part 'add_rest_state.dart';

part 'add_rest_bloc.freezed.dart';

class AddRestBloc extends Bloc<AddRest, AddRestState> {
  AddRestRepository repo;

  AddRestBloc(this.repo) : super(const Initial()) {
    on<AddRest>(_addRest);
  }

  void _addRest(AddRest event, Emitter<AddRestState> emit) async {
    try {
      emit(const AddRestState.loading());
      var res = await repo.addRest(event.body);
      emit(AddRestState.addRestInLoaded(msg: res.msg ?? ""));
    } catch (e) {
      emit(AddRestState.failure(message: e.toString()));
    }
  }
}

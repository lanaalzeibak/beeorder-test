part of 'add_rest_bloc.dart';

@freezed
class AddRestState with _$AddRestState {
  const factory AddRestState.addRestInLoaded({
    required String msg,
  }) = addRestInLoaded;

  const factory AddRestState.loading() = Loading;

  const factory AddRestState.failure({
    required String message,
  }) = Failure;

  const factory AddRestState.init() = Initial;
}

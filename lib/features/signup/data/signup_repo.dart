import 'package:bee_order_test/features/login/data/login_model/login_model.dart';
import 'package:bee_order_test/features/signup/data/signup_api.dart';

import 'signup_body_model/signup_body_model.dart';

class SignUpRepository {
  DioSignUp dio = DioSignUp();

  Future<LoginModel> checkSignUp(SignupBodyModel body) async {
    try {
      var res = await dio.signup(body);
      return res;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}

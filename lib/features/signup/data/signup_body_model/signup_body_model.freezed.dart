// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'signup_body_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SignupBodyModel _$SignupBodyModelFromJson(Map<String, dynamic> json) {
  return _SignupBodyModel.fromJson(json);
}

/// @nodoc
mixin _$SignupBodyModel {
  String get mobile => throw _privateConstructorUsedError;
  String get firstName => throw _privateConstructorUsedError;
  String get lastName => throw _privateConstructorUsedError;
  int get cityId => throw _privateConstructorUsedError;
  String get vehicle => throw _privateConstructorUsedError;
  String get licensePlate => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SignupBodyModelCopyWith<SignupBodyModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignupBodyModelCopyWith<$Res> {
  factory $SignupBodyModelCopyWith(
          SignupBodyModel value, $Res Function(SignupBodyModel) then) =
      _$SignupBodyModelCopyWithImpl<$Res, SignupBodyModel>;
  @useResult
  $Res call(
      {String mobile,
      String firstName,
      String lastName,
      int cityId,
      String vehicle,
      String licensePlate});
}

/// @nodoc
class _$SignupBodyModelCopyWithImpl<$Res, $Val extends SignupBodyModel>
    implements $SignupBodyModelCopyWith<$Res> {
  _$SignupBodyModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? mobile = null,
    Object? firstName = null,
    Object? lastName = null,
    Object? cityId = null,
    Object? vehicle = null,
    Object? licensePlate = null,
  }) {
    return _then(_value.copyWith(
      mobile: null == mobile
          ? _value.mobile
          : mobile // ignore: cast_nullable_to_non_nullable
              as String,
      firstName: null == firstName
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: null == lastName
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      cityId: null == cityId
          ? _value.cityId
          : cityId // ignore: cast_nullable_to_non_nullable
              as int,
      vehicle: null == vehicle
          ? _value.vehicle
          : vehicle // ignore: cast_nullable_to_non_nullable
              as String,
      licensePlate: null == licensePlate
          ? _value.licensePlate
          : licensePlate // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_SignupBodyModelCopyWith<$Res>
    implements $SignupBodyModelCopyWith<$Res> {
  factory _$$_SignupBodyModelCopyWith(
          _$_SignupBodyModel value, $Res Function(_$_SignupBodyModel) then) =
      __$$_SignupBodyModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String mobile,
      String firstName,
      String lastName,
      int cityId,
      String vehicle,
      String licensePlate});
}

/// @nodoc
class __$$_SignupBodyModelCopyWithImpl<$Res>
    extends _$SignupBodyModelCopyWithImpl<$Res, _$_SignupBodyModel>
    implements _$$_SignupBodyModelCopyWith<$Res> {
  __$$_SignupBodyModelCopyWithImpl(
      _$_SignupBodyModel _value, $Res Function(_$_SignupBodyModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? mobile = null,
    Object? firstName = null,
    Object? lastName = null,
    Object? cityId = null,
    Object? vehicle = null,
    Object? licensePlate = null,
  }) {
    return _then(_$_SignupBodyModel(
      mobile: null == mobile
          ? _value.mobile
          : mobile // ignore: cast_nullable_to_non_nullable
              as String,
      firstName: null == firstName
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: null == lastName
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      cityId: null == cityId
          ? _value.cityId
          : cityId // ignore: cast_nullable_to_non_nullable
              as int,
      vehicle: null == vehicle
          ? _value.vehicle
          : vehicle // ignore: cast_nullable_to_non_nullable
              as String,
      licensePlate: null == licensePlate
          ? _value.licensePlate
          : licensePlate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SignupBodyModel implements _SignupBodyModel {
  const _$_SignupBodyModel(
      {required this.mobile,
      required this.firstName,
      required this.lastName,
      required this.cityId,
      required this.vehicle,
      required this.licensePlate});

  factory _$_SignupBodyModel.fromJson(Map<String, dynamic> json) =>
      _$$_SignupBodyModelFromJson(json);

  @override
  final String mobile;
  @override
  final String firstName;
  @override
  final String lastName;
  @override
  final int cityId;
  @override
  final String vehicle;
  @override
  final String licensePlate;

  @override
  String toString() {
    return 'SignupBodyModel(mobile: $mobile, firstName: $firstName, lastName: $lastName, cityId: $cityId, vehicle: $vehicle, licensePlate: $licensePlate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SignupBodyModel &&
            (identical(other.mobile, mobile) || other.mobile == mobile) &&
            (identical(other.firstName, firstName) ||
                other.firstName == firstName) &&
            (identical(other.lastName, lastName) ||
                other.lastName == lastName) &&
            (identical(other.cityId, cityId) || other.cityId == cityId) &&
            (identical(other.vehicle, vehicle) || other.vehicle == vehicle) &&
            (identical(other.licensePlate, licensePlate) ||
                other.licensePlate == licensePlate));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, mobile, firstName, lastName, cityId, vehicle, licensePlate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SignupBodyModelCopyWith<_$_SignupBodyModel> get copyWith =>
      __$$_SignupBodyModelCopyWithImpl<_$_SignupBodyModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SignupBodyModelToJson(
      this,
    );
  }
}

abstract class _SignupBodyModel implements SignupBodyModel {
  const factory _SignupBodyModel(
      {required final String mobile,
      required final String firstName,
      required final String lastName,
      required final int cityId,
      required final String vehicle,
      required final String licensePlate}) = _$_SignupBodyModel;

  factory _SignupBodyModel.fromJson(Map<String, dynamic> json) =
      _$_SignupBodyModel.fromJson;

  @override
  String get mobile;
  @override
  String get firstName;
  @override
  String get lastName;
  @override
  int get cityId;
  @override
  String get vehicle;
  @override
  String get licensePlate;
  @override
  @JsonKey(ignore: true)
  _$$_SignupBodyModelCopyWith<_$_SignupBodyModel> get copyWith =>
      throw _privateConstructorUsedError;
}

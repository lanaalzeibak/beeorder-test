// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_body_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SignupBodyModel _$$_SignupBodyModelFromJson(Map<String, dynamic> json) =>
    _$_SignupBodyModel(
      mobile: json['mobile'] as String,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      cityId: json['cityId'] as int,
      vehicle: json['vehicle'] as String,
      licensePlate: json['licensePlate'] as String,
    );

Map<String, dynamic> _$$_SignupBodyModelToJson(_$_SignupBodyModel instance) =>
    <String, dynamic>{
      'mobile': instance.mobile,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'cityId': instance.cityId,
      'vehicle': instance.vehicle,
      'licensePlate': instance.licensePlate,
    };

// To parse this JSON data, do
//
//     final signupBodyModel = signupBodyModelFromJson(jsonString);

import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'signup_body_model.freezed.dart';

part 'signup_body_model.g.dart';

SignupBodyModel signupBodyModelFromJson(String str) =>
    SignupBodyModel.fromJson(json.decode(str));

String signupBodyModelToJson(SignupBodyModel data) =>
    json.encode(data.toJson());

@freezed
abstract class SignupBodyModel with _$SignupBodyModel {
  const factory SignupBodyModel({
    required String mobile,
    required String firstName,
    required String lastName,
    required int cityId,
    required String vehicle,
    required String licensePlate,
  }) = _SignupBodyModel;

  factory SignupBodyModel.fromJson(Map<String, dynamic> json) =>
      _$SignupBodyModelFromJson(json);
}

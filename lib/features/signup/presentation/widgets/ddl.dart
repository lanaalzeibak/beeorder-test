import 'package:bee_order_test/core/general_exports.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

class DDL extends StatefulWidget {
  final String hintText;

  const DDL({Key? key, required this.hintText}) : super(key: key);

  @override
  _DDLState createState() => _DDLState();
}

class _DDLState extends State<DDL> {
  final List<String> items = ['Damascus'];

  String? selectedValue;
  final TextEditingController textEditingController = TextEditingController();

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: DropdownButton2(
        buttonPadding: EdgeInsets.symmetric(horizontal: 15.sp),
        isExpanded: true,
        hint: Text(
          widget.hintText,
          style: TextStyle(
            fontSize: 15.sp,
            color: Theme.of(context).hintColor,
          ),
        ),
        items: items
            .map(
              (item) => DropdownMenuItem<String>(
                value: item,
                child: Text(item),
              ),
            )
            .toList(),
        value: selectedValue,
        onChanged: (value) {
          setState(() {
            selectedValue = value as String;
          });
        },
        searchController: textEditingController,
        searchInnerWidget: Padding(
          padding: const EdgeInsets.only(
            top: 8,
            bottom: 4,
            right: 8,
            left: 8,
          ),
          child: TextFormField(
            controller: textEditingController,
            decoration: InputDecoration(
              hintText: 'search ..',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(radius),
              ),
            ),
          ),
        ),
        buttonDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius),
          border: Border.all(
            width: 1,
            color: Colors.black.withOpacity(0.5),
          ),
        ),
        dropdownDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius),
        ),
        searchMatchFn: (item, searchValue) {
          return (item.value.toString().contains(searchValue));
        },
        onMenuStateChange: (isOpen) {
          if (!isOpen) {
            textEditingController.clear();
          }
        },
      ),
    );
  }
}

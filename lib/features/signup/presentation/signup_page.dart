import 'package:bee_order_test/core/utils/toast.dart';
import 'package:bee_order_test/features/signup/bloc/signup_bloc.dart';
import 'package:bee_order_test/features/signup/data/signup_body_model/signup_body_model.dart';
import 'package:bee_order_test/features/signup/data/signup_repo.dart';
import 'package:bee_order_test/features/signup/presentation/widgets/ddl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/general_exports.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SignUpBloc(SignUpRepository()),
      child: const SignUpView(),
    );
  }
}

class SignUpView extends StatefulWidget {
  const SignUpView({Key? key}) : super(key: key);

  @override
  State<SignUpView> createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  TextEditingController phoneController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController vehicleController = TextEditingController();
  TextEditingController plateController = TextEditingController();
  final _form = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Create account'),
      ),
      body: BlocConsumer<SignUpBloc, SignUpState>(
        listener: (context, state) async {
          if (state is signUpLoaded) {
            AutoRouter.of(context).push(const LoginInPageRoute());
          }
        },
        builder: (context, state) {
          if (state is Loading) {
            return const LoadingWidget();
          }
          return buildWidget();
        },
      ),
    );
  }

  Widget buildWidget() {
    return SingleChildScrollView(
      child: Form(
        key: _form,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          children: [
            SizedBox(height: 15.sp),
            Container(
              margin: EdgeInsets.all(15.sp),
              child: TextFormField(
                controller: phoneController,
                keyboardType: TextInputType.number,
                decoration: const InputDecoration()
                    .applyDefaults(theme.inputDecorationTheme)
                    .copyWith(labelText: 'Phone number', hintText: "09"),
                validator: (String? value) {
                  if (value != null) {
                    if (!value.startsWith('09') || value.length != 10) {
                      return "not valid";
                    }
                  } else {
                    return 'required';
                  }
                  return null;
                },
              ),
            ),
            Container(
              margin: EdgeInsets.all(15.sp),
              child: TextFormField(
                controller: firstNameController,
                decoration: const InputDecoration()
                    .applyDefaults(theme.inputDecorationTheme)
                    .copyWith(labelText: 'First name'),
                validator: (String? value) {
                  return validateRequired(value);
                },
              ),
            ),
            Container(
              margin: EdgeInsets.all(15.sp),
              child: TextFormField(
                controller: lastNameController,
                decoration: const InputDecoration()
                    .applyDefaults(theme.inputDecorationTheme)
                    .copyWith(labelText: 'Last name'),
                validator: (String? value) {
                  return validateRequired(value);
                },
              ),
            ),
            Container(
              margin: EdgeInsets.all(15.sp),
              child: TextFormField(
                controller: plateController,
                keyboardType: TextInputType.number,
                decoration: const InputDecoration()
                    .applyDefaults(theme.inputDecorationTheme)
                    .copyWith(labelText: 'License plate'),
                validator: (String? value) {
                  return validateRequired(value);
                },
              ),
            ),
            Container(
              margin: EdgeInsets.all(15.sp),
              child: TextFormField(
                controller: vehicleController,
                keyboardType: TextInputType.number,
                decoration: const InputDecoration()
                    .applyDefaults(theme.inputDecorationTheme)
                    .copyWith(labelText: 'Driver'),
                validator: (String? value) {
                  return validateRequired(value);
                },
              ),
            ),
            Container(
              margin: EdgeInsets.all(15.sp),
              child: const DDL(hintText: "Select city"),
            ),
            Container(
              margin: EdgeInsets.all(15.sp),
              height: 25.sp,
              width: 50.sp,
              child: ElevatedButton(
                onPressed: () {
                  callSignUp();
                },
                child: const Text('SignUp'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  callSignUp() {
    if (_form.currentState!.validate()) {
      final bloc = BlocProvider.of<SignUpBloc>(context);
      String phoneNumber = phoneController.text.trim().substring(1);
      SignupBodyModel body = SignupBodyModel(
        mobile: phoneNumber,
        firstName: firstNameController.text,
        lastName: lastNameController.text,
        cityId: 1,
        vehicle: vehicleController.text,
        licensePlate: plateController.text,
      );
      bloc.add(SignUp(body: body));
    }
  }

  @override
  void dispose() {
    lastNameController.dispose();
    firstNameController.dispose();
    phoneController.dispose();
    vehicleController.dispose();
    plateController.dispose();
    super.dispose();
  }
}

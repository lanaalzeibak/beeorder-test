part of 'signup_bloc.dart';


@immutable
abstract class SignUpEvent {}

class SignUp extends SignUpEvent {
  final SignupBodyModel body;

  SignUp({required this.body});
}

part of 'signup_bloc.dart';

@freezed
class SignUpState with _$SignUpState {
  const factory SignUpState.signUpLoaded() = signUpLoaded;

  const factory SignUpState.loading() = Loading;

  const factory SignUpState.failure({
    required String message,
  }) = Failure;

  const factory SignUpState.init() = Initial;
}


import 'package:bee_order_test/core/utils/toast.dart';
import 'package:bee_order_test/features/signup/data/signup_body_model/signup_body_model.dart';
import 'package:bee_order_test/features/signup/data/signup_repo.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

part 'singup_event.dart';
part 'singup_state.dart';
part 'signup_bloc.freezed.dart';


class SignUpBloc extends Bloc<SignUp, SignUpState> {
  SignUpRepository repo;

  SignUpBloc(this.repo) : super(const Initial()) {
    on<SignUp>(_checkSignUp);
  }

  void _checkSignUp(SignUp event, Emitter<SignUpState> emit) async {
    try {
      emit(const SignUpState.loading());
      var res = await repo.checkSignUp(event.body);
      showToast(msg: res.message ?? "", isError: false);
      emit(const SignUpState.signUpLoaded());
    } catch (e) {
      emit(SignUpState.failure(message: e.toString()));
    }
  }
}

import 'dart:convert';

import 'package:bee_order_test/core/notification/init_app_firebase.dart';
import 'package:bee_order_test/gen/assets.gen.dart';
import 'package:bee_order_test/my_app.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:json_theme/json_theme.dart';

final navigatorKey = GlobalKey<NavigatorState>();
late ThemeData theme;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initAppNotification();
  
  var themeStr = await rootBundle.loadString(Assets.appTheme);
  var themeJson = json.decode(themeStr);
  theme = ThemeDecoder.decodeThemeData(themeJson) ?? ThemeData();

  runApp(MyApp());
}
